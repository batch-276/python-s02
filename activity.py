while True:
	year = int(input("Enter a year: \n"))
	if year <= 0 :
		print("Invalid input! Year must be a positive integers.")
	else:
		break

if year % 4 == 0 and year % 100 != 0 or year % 400 == 0 :
	print(f"{year}, is a leap year")
else:
	print(f"{year}, is not a leap year")





while True:
	
	rows = int(input("Enter number of rows: \n"))
	columns = int(input("Enter number of columns:\n"))

	if rows <= 0 or columns <= 0:
		print("Invalid input! Rows and columns must be positive integers.")
	else: break

for i in range(rows):
	row = ""
	for j in range(columns):
		row += "*"
	print(row)